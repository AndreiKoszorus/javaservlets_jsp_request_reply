<%--
  Created by IntelliJ IDEA.
  User: Admin
  Date: 30.10.2018
  Time: 22:56
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Client page</title>
    <style type="text/css">
        .paddingBtm{
            padding-bottom: 20px;
        }
    </style>
</head>
<body>
<h2>Client Page</h2>
<form id="showFlights" name="flightForm" method="get" action="/userServlet">
    <div>
        <span> View Flights: </span><input type="submit" value="View"/>
    </div>
</form>
<form id="getLocalTime" name="LocalTimeForm" method="post" action="/localTimeServlet">
    <div class="paddingBtm">
        <span> Flight number: </span><input type="text" name="flightNumber"/>
    </div>
    <div id="getLocalTimeBtn">
        <input type="submit" value="Get local time">
    </div>
</form>
</body>
</html>
