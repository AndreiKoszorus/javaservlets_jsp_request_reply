<%--
  Created by IntelliJ IDEA.
  User: Admin
  Date: 30.10.2018
  Time: 22:56
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
    <style type="text/css">
        .paddingBtm{
            padding-bottom: 20px;
        }
    </style>
</head>
<body>
<h2>Admin page</h2>

<h3>Add flight</h3>
<form name="AddFlightForm" method="post" action="/admin">
    <div class="paddingBtm">
        <span>Flight number: </span><input type="text" name="addFlightNumber"/>
    </div>
    <div class="paddingBtm">
        <span>Airplane type: </span><input type="text" name="addFlightType"/>
    </div>
    <div class="paddingBtm">
        <span>Departure city: </span><input type="text" name="addDepartureCity"/>
    </div>
    <div class="paddingBtm">
        <span>Departure date: </span><input type="text" name="addDepartureDate" placeholder="MMMM d, YYYY hh:mm"/>
    </div>
    <div class="paddingBtm">
        <span>Arrival city: </span><input type="text" name="addArrivalCity"/>
    </div>
    <div class="paddingBtm">
        <span>Arrival date: </span><input type="text" name="addArrivalDate" placeholder="MMMM d, YYYY hh:mm"/>
    </div>
    <div>
        <input type="submit" value="Add"/>
    </div>
</form>

<h3>Update flight</h3>
<form name="UpdateFlightForm" method="post" action="/updateServlet">
    <div class="paddingBtm">
        <span>Flight id: </span><input type="text" name="idFlightNumber"/>
    </div>
    <div class="paddingBtm">
        <span>Flight number: </span><input type="text" name="updateFlightNumber"/>
    </div>
    <div class="paddingBtm">
        <span>Airplane type: </span><input type="text" name="updateFlightType"/>
    </div>
    <div class="paddingBtm">
        <span>Departure city: </span><input type="text" name="updateDepartureCity"/>
    </div>
    <div class="paddingBtm">
        <span>Departure date: </span><input type="text" name="updateDepartureDate" placeholder="MMMM d, YYYY hh:mm"/>
    </div>
    <div class="paddingBtm">
        <span>Arrival city: </span><input type="text" name="updateArrivalCity"/>
    </div>
    <div class="paddingBtm">
        <span>Arrival date: </span><input type="text" name="updateArrivalDate" placeholder="MMMM d, YYYY hh:mm"/>
    </div>
    <div>
        <input type="submit" value="Update"/>
    </div>
</form>

<h3>Get flight</h3>
<form name="GetFlightForm" method="get" action="/admin">
    <div class="paddingBtm">
        <span>Flight number: </span><input type="text" name="getFlightNumber"/>
    </div>
    <div>
        <input type="submit" value="Get"/>
    </div>
</form>

<h3>Delete flight</h3>
<form name="DeleteFlightForm" method="post" action="/deleteServlet">
    <div class="paddingBtm">
        <span>Flight id: </span><input type="text" name="deleteFlightId"/>
    </div>
    <div>
        <input type="submit" value="Delete"/>
    </div>
</form>


</body>
</html>
