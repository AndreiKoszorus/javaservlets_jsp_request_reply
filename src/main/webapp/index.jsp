
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Flight manager</title>
    <style type="text/css">
        .paddingBtm {
            padding-bottom: 12px;
        }
    </style>
</head>
<body>
<form method="post" action="/loginServlet">
    <div id="usernameDiv" class="paddingBtm">
        <span id="user"> Username: </span><input type="email" name="email">
    </div>
    <div id="passwordDiv" class="paddingBtm">
        <span id="pass"> Password: </span><input type="password" name="password">
    </div>

    <div id="loginBtn">
        <input id="btn" type="submit" value="Login" />
    </div>
</form>
</body>
</html>
