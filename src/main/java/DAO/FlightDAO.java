package DAO;


import Models.Flight;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;

import javax.persistence.Query;
import java.util.List;

/**
 * Created by Admin on 29.10.2018.
 */
public class FlightDAO {
    private final StandardServiceRegistry registry = (new StandardServiceRegistryBuilder()).configure().build();
    private SessionFactory sessionFactory;
    private static final Log LOGGER = LogFactory.getLog(FlightDAO.class);

    public FlightDAO(){
        try {
            this.sessionFactory = (new MetadataSources(this.registry)).buildMetadata().buildSessionFactory();
        } catch (Exception var2) {
            StandardServiceRegistryBuilder.destroy(this.registry);
            System.err.println("Error in hibernate: ");
            var2.printStackTrace();
        }
    }
    @SuppressWarnings("Duplicates")
    public void createFlight(Flight flight){
        Session session=sessionFactory.openSession();
        Transaction tx=null;
        try{
            tx=session.beginTransaction();
            session.save(flight);
            tx.commit();
        }
        catch (HibernateException ex){
            if(tx!=null){
                tx.rollback();
            }
            LOGGER.error("",ex);
        }
        finally {
            session.close();
        }


    }
    @SuppressWarnings("unchecked")
    public Flight readFlight(int flightId){
        Session session=sessionFactory.openSession();
        List<Flight> flights=null;
        Transaction tx=null;

        try{
            tx=session.beginTransaction();
            Query query = session.createQuery("FROM Flight WHERE id =: id");
            query.setParameter("id",flightId);
            flights= query.getResultList();
            tx.commit();

        }
        catch(HibernateException ex){
            if(tx != null){
                tx.rollback();
            }
            LOGGER.error("",ex);

        }
        finally {
            session.close();
        }

        return flights !=null && !flights.isEmpty() ? flights.get(0): null;
    }

    public Flight readFlightByNumber(int flightNr){
        Session session=sessionFactory.openSession();
        List<Flight> flights=null;
        Transaction tx=null;

        try{
            tx=session.beginTransaction();
            Query query = session.createQuery("FROM Flight WHERE flightNumber =: flightNr");
            query.setParameter("flightNr",flightNr);
            flights= query.getResultList();
            tx.commit();

        }
        catch(HibernateException ex){
            if(tx != null){
                tx.rollback();
            }
            LOGGER.error("",ex);

        }
        finally {
            session.close();
        }

        return flights !=null && !flights.isEmpty() ? flights.get(0): null;
    }

    @SuppressWarnings("unchecked")
    public List<Flight> getAllFLights(){
        Session session = sessionFactory.openSession();
        List<Flight> flights = null;
        Transaction tx= null;

        try{
            tx=session.beginTransaction();
            flights = session.createQuery("FROM Flight").list();
        }
        catch (HibernateException ex){
            if(tx != null){
                tx.rollback();
            }
            LOGGER.error("",ex);
        }
        finally {
            session.close();
        }

        return flights;
    }
    @SuppressWarnings("unchecked" )
    public void updateFlight(int id, Flight flight){
        Session session = sessionFactory.openSession();
        List<Flight> flights= null;
        Transaction tx= null;


        try{
            tx = session.beginTransaction();
            flight.setId(id);
            session.update(flight);
            tx.commit();
        }
        catch (HibernateException ex){
            if(tx != null){
                tx.rollback();
            }
            LOGGER.error("", ex);
        }
        finally {
            session.close();
        }
    }
    @SuppressWarnings("unchecked" )
    public void deleteFlight(int id){
        Session session = sessionFactory.openSession();
        Transaction tx = null;
        List<Flight> flights= null;
        try {
            tx = session.beginTransaction();
            Query query = session.createQuery("FROM Flight where id = :id");
            query.setParameter("id", id);
            flights = query.getResultList();
            Flight deletedFlight = flights.get(0);
            session.delete(deletedFlight);
            tx.commit();

        }
        catch (HibernateException ex){
            if(tx != null){
                tx.rollback();
            }
            LOGGER.error("",ex);
        }
        finally {
            session.close();
        }
    }


}
