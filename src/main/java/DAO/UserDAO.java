package DAO;

import Models.User;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;

import javax.persistence.Query;
import java.util.List;

public class UserDAO {

    private final StandardServiceRegistry registry = (new StandardServiceRegistryBuilder()).configure().build();
    private SessionFactory sessionFactory;
    private static final Log LOGGER = LogFactory.getLog(UserDAO.class);

    public UserDAO(){
        try {
            this.sessionFactory = (new MetadataSources(this.registry)).buildMetadata().buildSessionFactory();
        } catch (Exception var2) {
            StandardServiceRegistryBuilder.destroy(this.registry);
            System.err.println("Error in hibernate: ");
            var2.printStackTrace();
        }
    }

    public void createUser(User user){
        Session session=sessionFactory.openSession();
        Transaction tx=null;
        try{
            tx=session.beginTransaction();
            session.save(user);
            tx.commit();
        }
        catch (HibernateException ex){
            if(tx!=null){
                tx.rollback();
            }
            LOGGER.error("",ex);
        }
        finally {
            session.close();
        }
    }
    @SuppressWarnings("unchecked")
    public User getUser(String username){
        Session session= sessionFactory.openSession();
        Transaction tx = null;
        List<User> users=null;
        try{
            tx=session.beginTransaction();
            Query query = session.createQuery("FROM User WHERE username = :username");
            query.setParameter("username",username);
            users= query.getResultList();
            tx.commit();

        }
        catch (HibernateException ex){
            if(tx!=null){
                tx.rollback();
            }
            LOGGER.error("",ex);
        }
        finally {
            session.close();
        }

        return users != null && !users.isEmpty() ? users.get(0): null;
    }



}
