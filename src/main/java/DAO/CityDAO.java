package DAO;

import Models.City;
import Models.Flight;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;

import javax.persistence.Query;
import java.util.List;

public class CityDAO {
    private final StandardServiceRegistry registry = (new StandardServiceRegistryBuilder()).configure().build();
    private SessionFactory sessionFactory;
    private static final Log LOGGER = LogFactory.getLog(FlightDAO.class);

    public CityDAO() {
        try {
            this.sessionFactory = (new MetadataSources(this.registry)).buildMetadata().buildSessionFactory();
        } catch (Exception var2) {
            StandardServiceRegistryBuilder.destroy(this.registry);
            System.err.println("Error in hibernate: ");
            var2.printStackTrace();
        }
    }

    public void createCity(City city) {
        Session session = sessionFactory.openSession();
        Transaction tx = null;
        try {
            tx = session.beginTransaction();
            session.save(city);
            tx.commit();
        } catch (HibernateException ex) {
            if (tx != null) {
                tx.rollback();
            }
            LOGGER.error("", ex);
        } finally {
            session.close();
        }
    }
    @SuppressWarnings("unchecked")
    public City readCity(String cityName){
        Session session=sessionFactory.openSession();
        List<City> cities=null;
        Transaction tx=null;

        try{
            tx=session.beginTransaction();
            Query query = session.createQuery("FROM City WHERE name =: name");
            query.setParameter("name",cityName);
            cities= query.getResultList();
            tx.commit();

        }
        catch(HibernateException ex){
            if(tx != null){
                tx.rollback();
            }
            LOGGER.error("",ex);

        }
        finally {
            session.close();
        }

        return cities !=null && !cities.isEmpty() ? cities.get(0): null;
    }
}
