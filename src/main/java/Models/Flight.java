package Models;

;import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by Admin on 29.10.2018.
 */
@Entity
@Table(
        name = "FLIGHTS"
)
public class Flight {
    private int id;
    private int flightNumber;
    private String airplaneType;
    private String departureCity;
    private Date departureDate;
    private Date arrivalDate;
    private String arrivalCity;

    public Flight(){

    }
    public Flight(int flightNumber, String airplaneType, String departureCity, Date departureDate, Date arrivalDate, String arrivalCity) {
        this.flightNumber = flightNumber;
        this.airplaneType = airplaneType;
        this.departureCity = departureCity;
        this.departureDate = departureDate;
        this.arrivalDate = arrivalDate;
        this.arrivalCity = arrivalCity;
    }

    @Id
    @GeneratedValue(
            generator = "increment"
    )
    @GenericGenerator(
            name = "increment",
            strategy = "increment"
    )
    public int getId() {
        return id;
    }

    public int getFlightNumber() {
        return flightNumber;
    }

    public String getAirplaneType() {
        return airplaneType;
    }

    public String getDepartureCity() {
        return departureCity;
    }

    public Date getDepartureDate() {
        return departureDate;
    }

    public Date getArrivalDate() {
        return arrivalDate;
    }

    public String getArrivalCity() {
        return arrivalCity;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setFlightNumber(int flightNumber) {
        this.flightNumber = flightNumber;
    }

    public void setAirplaneType(String airplaneType) {
        this.airplaneType = airplaneType;
    }

    public void setDepartureCity(String departureCity) {
        this.departureCity = departureCity;
    }

    public void setDepartureDate(Date departureDate) {
        this.departureDate = departureDate;
    }

    public void setArrivalDate(Date arrivalDate) {
        this.arrivalDate = arrivalDate;
    }

    public void setArrivalCity(String arrivalCity) {
        this.arrivalCity = arrivalCity;
    }
}
