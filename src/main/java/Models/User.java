package Models;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(
        name = "USERS"
)
public class User {
    private int id;
    private String username;
    private String password;
    private boolean role;

    public User(){

    }
    public User(String username, String password, boolean role) {
        this.username = username;
        this.password = password;
        this.role = role;
    }
    @Id
    @GeneratedValue(
            generator = "increment"
    )
    @GenericGenerator(
            name = "increment",
            strategy = "increment"
    )

    public int getId() {
        return id;
    }
    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public boolean isRole() {
        return role;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setRole(boolean role) {
        this.role = role;
    }
}
