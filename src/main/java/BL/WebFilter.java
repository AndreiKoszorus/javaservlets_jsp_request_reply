package BL;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@javax.servlet.annotation.WebFilter(urlPatterns = {"/*"})
public class WebFilter implements Filter {


    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;
        HttpSession session = request.getSession();
        StringBuffer path = request.getRequestURL();
        String[] split=path.substring(request.getContextPath().length()).split("/");
        System.out.println(path+" "+split.length);
        System.out.println(split[split.length-1]);
        if(split.length == 0){
            filterChain.doFilter(servletRequest,servletResponse);
            return ;

        }

        if(split[split.length-1].equals("admin.jsp") && (session.getAttribute("role")==null || session.getAttribute("role").equals("false"))){
            response.sendRedirect("/index.jsp");
        }
        else if(split[split.length-1].equals("user.jsp") && (session.getAttribute("role")==null || session.getAttribute("role").equals("true"))){
            response.sendRedirect("/index.jsp");
        }
        filterChain.doFilter(request,response);


    }

    @Override
    public void destroy() {

    }
}
