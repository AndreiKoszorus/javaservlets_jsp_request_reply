package BL;

import DAO.UserDAO;
import Models.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(
        urlPatterns = {"/loginServlet"}
)
public class LoginServlet extends HttpServlet {
    private UserDAO userDAO;

    public LoginServlet(){
        userDAO = new UserDAO();
    }

    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

    }

    @Override
   public void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String username=req.getParameter("email");
        String password=req.getParameter("password");
        PrintWriter out = resp.getWriter();
        HttpSession session=req.getSession();

        User user=userDAO.getUser(username);
        if(user!=null) {

            session.setAttribute("role", String.valueOf(user.isRole()));
            System.out.println(String.valueOf(user.isRole()));

            if (user.getPassword().equals(password) && user.getUsername().equals(username) && user.isRole() ) {
                resp.sendRedirect("/admin.jsp");
            }
            else if(user.getPassword().equals(password) && user.getUsername().equals(username) && !user.isRole()){
                resp.sendRedirect("/user.jsp");
            }
            else{
                out.write("<p style='color: red; font-size: larger;'>Password doesn't match</p>");
            }
        }
        else{
            out.write("<p style='color: red; font-size: larger;'>Username doesn't exists</p>");
        }
        out.write("</div></body></html>");
        out.close();


    }
}
