package BL;

import DAO.CityDAO;
import DAO.FlightDAO;
import Models.City;
import Models.Flight;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.URL;

@WebServlet(
        urlPatterns = {"/localTimeServlet"}
)
public class LocalTimeServlet extends HttpServlet {
    private FlightDAO flightDAO;
    private CityDAO cityDAO;
    public LocalTimeServlet(){
        flightDAO = new FlightDAO();
        cityDAO = new CityDAO();
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String flightNr = req.getParameter("flightNumber");
        String numberCheck="[0-9]{3}";
        PrintWriter out = resp.getWriter();
        out.write("<html><body><div id=\"serlvetResponse\" style=\"text-align: center;\">");
        try {
            if (flightNr.matches(numberCheck)) {
                int flightNumber = Integer.parseInt(flightNr);
                Flight flight = flightDAO.readFlightByNumber(flightNumber);
                System.out.println(flight);


                if (flight != null) {
                    City departureCity = cityDAO.readCity(flight.getDepartureCity());
                    City arrivalCity = cityDAO.readCity(flight.getArrivalCity());

                    String urlString = String.format("http://new.earthtools.org/timezone/%f/%f", departureCity.getLatitude(), departureCity.getLongitude());
                    String goodUrl = urlString.replace(',','.');
                    DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
                    DocumentBuilder db = dbf.newDocumentBuilder();
                    Document doc = db.parse(new URL(goodUrl).openStream());

                    NodeList nodeList1 = doc.getElementsByTagName("localtime");
                    Element departureTime = (Element) nodeList1.item(0);


                    String urlString1 = String.format("http://new.earthtools.org/timezone/%f/%f", arrivalCity.getLatitude(), arrivalCity.getLongitude());
                    String goodUrl1 = urlString1.replace(',','.');
                    DocumentBuilderFactory dbf1 = DocumentBuilderFactory.newInstance();
                    DocumentBuilder db1 = dbf1.newDocumentBuilder();
                    Document doc1 = db1.parse(new URL(goodUrl1).openStream());

                    NodeList nodeList = doc1.getElementsByTagName("localtime");
                    Element arrivalTime = (Element) nodeList.item(0);
                    out.write("<html><body><div id=\"serlvetResponse\" style=\"text-align: center;\">");
                    out.write("<p>Flight number: " + flight.getFlightNumber() + "</p>");
                    out.write("<p> Departure city: " + flight.getDepartureCity() + " with local time :" + departureTime.getTextContent() + "</p>");
                    out.write("<p> Arrival city: " + flight.getArrivalCity() + " with local time :" + arrivalTime.getTextContent() + "</p>");
                    out.write("</div></body></html>");


                } else {
                    out.write("<p style='color: red; font-size: larger;'>No flight with this number</p>");
                }
            } else {
                out.write("<p style='color: red; font-size: larger;'>Incorrect flight number</p>");
            }
            out.write("</div></body></html>");
        }
        catch (ParserConfigurationException|SAXException ex){
            System.out.println(ex.getMessage());
        }



    }
}
