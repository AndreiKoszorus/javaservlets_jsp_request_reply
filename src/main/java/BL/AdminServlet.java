package BL;

import DAO.FlightDAO;
import Models.Flight;
import org.hibernate.HibernateException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

@WebServlet(
        urlPatterns = {"/admin"}
)
public class AdminServlet extends HttpServlet {
    private FlightDAO flightDAO;
    public AdminServlet(){
        flightDAO = new FlightDAO();
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            String numberCheck="[0-9]{3}";
            PrintWriter out = resp.getWriter();
            DateFormat format = new SimpleDateFormat("MMMM d, yyyy hh:mm", Locale.ENGLISH);
            out.write("<html><body><div id='serlvetResponse' style='text-align: center;'>");
            if(req.getParameter("addFlightNumber").matches(numberCheck)) {
                int flightNumber = Integer.parseInt(req.getParameter("addFlightNumber"));
                String flightType = req.getParameter("addFlightType");
                String departureCity = req.getParameter("addDepartureCity");
                Date departureDate = format.parse(req.getParameter("addDepartureDate"));
                String arrivalCity = req.getParameter("addArrivalCity");
                Date arrivalDate = format.parse(req.getParameter("addArrivalDate"));

                Flight addFlight = new Flight(flightNumber, flightType, departureCity, departureDate, arrivalDate, arrivalCity);

                flightDAO.createFlight(addFlight);


                out.write("<h2 style='color: green;'>Successfully added flight</h2>");
            }
            else{
                out.write("<h2 style='color: green;'>Invalid flight number !</h2>");
            }
            out.write("</div></body></html>");
        }
        catch (ParseException | HibernateException ex){
            System.out.print(ex.getMessage());
        }

    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        try{
            PrintWriter out = resp.getWriter();
            String checkNumber = "[0-9]{3}";
            String flightNumberString = req.getParameter("getFlightNumber");
            out.write("<html><body><div id='serlvetResponse' style='text-align: center;'>");
            if(flightNumberString.matches(checkNumber)){
                Flight checkFlight = flightDAO.readFlightByNumber(Integer.parseInt(flightNumberString));

                if(checkFlight!=null){

                    out.write("<table><tr><th>Flight number</th><th>Airplane type</th><th>Departure city</th><th>Departure date</th><th>Arrival City</th><th>Arrival Date</th></tr>");
                    out.write("<tr><td>" + checkFlight.getFlightNumber() + "</td><td>" + checkFlight.getAirplaneType() + "</td><td>" + checkFlight.getDepartureCity() + "</td><td>" + checkFlight.getDepartureDate() + "</td><td>" + checkFlight.getArrivalCity() + "</td><td>" + checkFlight.getArrivalDate() + "</td></tr>");

                }
                else{

                    out.write("<h2 style='color: red;'>Flight doesn't exist !</h2>");

                }
            }
            else {
                out.write("<h2 style='color: red;'>Invalid flight number !</h2>");
            }
            out.write("</div></body></html>");

        }
        catch (HibernateException ex){
            System.out.print(ex.getMessage());
        }



    }
}
