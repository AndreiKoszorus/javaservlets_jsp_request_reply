package BL;

import DAO.FlightDAO;
import Models.Flight;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

@WebServlet(
        urlPatterns = {"/updateServlet"}
)
public class UpdateServlet extends HttpServlet {
    private FlightDAO flightDAO;

    public UpdateServlet(){
        flightDAO = new FlightDAO();
    }
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        try {
            String checkNumber = "[0-9]+";
            String idString = req.getParameter("idFlightNumber");
            PrintWriter out = resp.getWriter();
            DateFormat format = new SimpleDateFormat("MMMM d, yyyy hh:mm", Locale.ENGLISH);

            out.write("<html><body><div id='serlvetResponse' style='text-align: center;'>");

            if (idString.matches(checkNumber)) {
                int id = Integer.parseInt(idString);

                Flight searchFlight = flightDAO.readFlight(id);

                if (searchFlight != null) {
                    int flightNumber = Integer.parseInt(req.getParameter("updateFlightNumber"));
                    String flightType = req.getParameter("updateFlightType");
                    String departureCity = req.getParameter("updateDepartureCity");
                    Date departureDate = format.parse(req.getParameter("updateDepartureDate"));
                    String arrivalCity = req.getParameter("updateArrivalCity");
                    Date arrivalDate = format.parse(req.getParameter("updateArrivalDate"));

                    Flight updateFlight = new Flight(flightNumber, flightType, departureCity, departureDate, arrivalDate, arrivalCity);

                    flightDAO.updateFlight(id,updateFlight);

                    out.write("<h2 style='color: green;'>Successfully updated flight</h2>");
                } else {

                    out.write("<h2 style='color: red;'>Flight with that id doesn't exist</h2>");
                }

            } else {
                out.write("<h2 style='color: red;'>Invalid id</h2>");
            }
            out.write("</div></body></html>");
        }
        catch (ParseException ex){
            System.out.print(ex.getMessage());
        }

    }
}
