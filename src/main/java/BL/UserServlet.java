package BL;

import DAO.FlightDAO;
import Models.Flight;
import org.hibernate.HibernateException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

@WebServlet(
        urlPatterns = {"/userServlet"}
)
public class UserServlet extends HttpServlet {

    private FlightDAO flightDAO;

    public UserServlet(){
        flightDAO = new FlightDAO();
    }
    @Override
    public void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html");
        System.out.println("ENTER");

        try{
            List<Flight> flights = flightDAO.getAllFLights();
            PrintWriter out = resp.getWriter();
            out.write("<html><body><div id=\"serlvetResponse\" align=\"center\">");
            if(flights!=null) {
                out.write("<table><tr><th>Flight number</th><th>Airplane type</th><th>Departure city</th><th>Departure date</th><th>Arrival City</th><th>Arrival Date</th></tr>");
                for (Flight flight : flights) {

                    out.write("<tr><td>" + flight.getFlightNumber() + "</td><td>" + flight.getAirplaneType() + "</td><td>" + flight.getDepartureCity() + "</td><td>" + flight.getDepartureDate() + "</td><td>" + flight.getArrivalCity() + "</td><td>" + flight.getArrivalDate() + "</td></tr>");
                }
                out.write("</table>");
                out.write("</div></body></html>");
            }
            else{
                out.write("<p style='color: red; font-size: larger;'>No flights to show</p>");
                out.write("</div></body></html>");
            }
        }
        catch (HibernateException ex){
            System.out.println("ERROR");
        }
    }

}
