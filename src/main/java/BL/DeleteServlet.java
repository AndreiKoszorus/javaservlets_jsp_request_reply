package BL;

import DAO.FlightDAO;
import Models.Flight;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet(
        urlPatterns = {"/deleteServlet"}
)
public class DeleteServlet extends HttpServlet {
    private FlightDAO flightDAO;
    public DeleteServlet(){
        flightDAO = new FlightDAO();
    }
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String checkNumber = "[0-9]+";
        String idString = req.getParameter("deleteFlightId");
        PrintWriter out = resp.getWriter();
        int id;
        out.write("<html><body><div id='serlvetResponse' style='text-align: center;'>");
        if(idString.matches(checkNumber)){
            id= Integer.parseInt(idString);
            Flight checkFlight = flightDAO.readFlight(id);

            if(checkFlight!=null){
                flightDAO.deleteFlight(id);
                out.write("<h2 style='color: green;'>Successfully added flight</h2>");
            }
            else{
                out.write("<h2 style='color: red;'>Flight with that id doesn't exist</h2>");
            }
        }
        else{
            out.write("<h2 style='color: red;'>Invalid id</h2>");
        }
        out.write("</div></body></html>");


    }
}
